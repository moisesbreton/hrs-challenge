<?php

namespace HRS\App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Http\Discovery\StreamFactoryDiscovery;

class Patients
{
    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function index(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patients',
            'action' => 'index',
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function get(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patients',
            'action' => 'get',
            'id' => $args['id'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function create(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patients',
            'action' => 'create',
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function update(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patients',
            'action' => 'update',
            'id' => $args['id'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function delete(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patients',
            'action' => 'delete',
            'id' => $args['id'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }
}