<?php

namespace HRS\App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Http\Discovery\StreamFactoryDiscovery;

class PatientsMetrics
{
    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function index(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patientsmetrics',
            'action' => 'index',
            'id' => $args['id'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function get(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patientsmetrics',
            'action' => 'get',
            'id' => $args['id'],
            'id2' => $args['id2'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function create(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patientsmetrics',
            'action' => 'create',
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function update(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patientsmetrics',
            'action' => 'update',
            'id' => $args['id'],
            'id2' => $args['id2'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param ServerRequestInterface $req
     * @param ResponseInterface $res
     * @param array $args
     * @return ResponseInterface
     */
    public function delete(ServerRequestInterface $req, ResponseInterface $res, array $args)
    {
        $data = [
            'controller' => 'patientsmetrics',
            'action' => 'delete',
            'id' => $args['id'],
            'id2' => $args['id2'],
            'status' => 'OK'
        ];

        $stream = StreamFactoryDiscovery::find();

        return $res
            ->withBody($stream->createStream(json_encode($data)))
            ->withStatus(200, 'OK')
            ->withHeader('Content-Type', 'application/json');
    }
}