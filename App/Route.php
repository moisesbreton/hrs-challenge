<?php

namespace HRS\App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use GuzzleHttp\Psr7\ServerRequest;
use Http\Discovery\StreamFactoryDiscovery;

class Route
{
    private static $routes = [];

    /**
     * @var $request ServerRequestInterface
     */
    private static $request;

    /**
     * @var $request ResponseInterface
     */
    private static $response;

    public static function init()
    {
        $request = ServerRequest::fromGlobals();
        $contentType = $request->getHeaderLine('Content-Type');
        $bodyStr = $request->getBody()->getContents();
        if (strpos($contentType, 'application/json') === 0) {
            $request = $request->withParsedBody(json_decode($bodyStr, true));
        } elseif (strpos($contentType, 'application/x-www-form-urlencoded') === 0) {
            parse_str($bodyStr, $body);
            $request = $request->withParsedBody($body);
        }

        // Unfortunately I did not find a nice and clean way to reset the stream so, that next call to
        // getBody->getContents() returns something. So, I basically rebuild the stream...
        if (!empty($bodyStr)) {
            $request = $request->withBody(\Http\Discovery\StreamFactoryDiscovery::find()->createStream($bodyStr));
        }

        $response = \Http\Discovery\MessageFactoryDiscovery::find()->createResponse();

        self::$request = $request;
        self::$response = $response;
    }

    /**
     * @param string $url
     */
    public static function resource(string $url)
    {
        if (empty($url)) {
            return;
        }

        $nested = false;
        $segments = explode('.', $url);
        $controller = ucfirst($segments[0]);

        if (isset($segments[1])) {
            $controller .= ucfirst($segments[1]);
            $nested = true;
        }

        $controller = "HRS\\App\\Controllers\\".$controller;

        $ctrl = new $controller;

        self::map(
            ['GET', 'POST'],
            !$nested ? "/$segments[0]" : "/{$segments[0]}/{id}/{$segments[1]}",
            function (ServerRequestInterface $request, ResponseInterface $response, $args) use ($ctrl) {
                $method = strtolower($request->getMethod());
                $action = 'index';
                if ($method === 'post') {
                    $action = 'create';
                }
                return $ctrl->$action($request, $response, $args);
            }
        );

        self::get(!$nested ? "/{$segments[0]}/{id}" : "/{$segments[0]}/{id}/{$segments[1]}/{id2}",
            function (ServerRequestInterface $request, ResponseInterface $response, $args) use ($ctrl) {
                $action = strtolower($request->getMethod());
                return $ctrl->$action($request, $response, $args);
            }
        );

        self::map(
            ['PATCH', 'DELETE'],
            !$nested ? "/$segments[0]/{id}" : "/{$segments[0]}/{id}/{$segments[1]}/{id2}",
            function (ServerRequestInterface $request, ResponseInterface $response, $args) use ($ctrl) {
                $action = strtolower($request->getMethod());
                if ($request->getMethod() === 'PATCH') {
                    $action = 'update';
                }
                return $ctrl->$action($request, $response, $args);
            }
        );
    }

    /**
     * @param array $methods
     * @param string $pattern
     * @param $callable
     */
    public static function add(array $methods, string $pattern, $callable)
    {
        $pattern = '/'.trim($pattern, '/');
        foreach ($methods as $method) {
            self::$routes[$method][] = [
                'pattern' => $pattern,
                'callback' => $callable,
            ];
        }
    }

    /**
     * @param array $methods
     * @param string $pattern
     * @param $callable
     */
    public static function map(array $methods, string $pattern, $callable)
    {
        self::add($methods, $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function get(string $pattern, $callable)
    {
        self::add(['GET'], $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function post(string $pattern, $callable)
    {
        self::add(['POST'], $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function patch(string $pattern, $callable)
    {
        self::add(['PATCH'], $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function delete(string $pattern, $callable)
    {
        self::add(['DELETE'], $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function put(string $pattern, $callable)
    {
        self::add(['PUT'], $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function option(string $pattern, $callable)
    {
        self::add(['OPTIONS'], $pattern, $callable);
    }

    /**
     * @param string $pattern
     * @param $callable
     */
    public static function any(string $pattern, $callable)
    {
        self::add(
            ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH', 'HEAD'],
            $pattern,
            $callable
        );
    }

    /**
     * @param $pattern
     * @return bool|string
     */
    private static function getRegex($pattern){
        if (preg_match('/[^-:\/_{}()a-zA-Z\d]/', $pattern))
            return false; // Invalid pattern

        // Turn "(/)" into "/?"
        $pattern = preg_replace('#\(/\)#', '/?', $pattern);

        // Create capture group for ":parameter"
        $allowedParamChars = '[a-zA-Z0-9\_\-]+';
        $pattern = preg_replace(
            '/:(' . $allowedParamChars . ')/',   # Replace ":parameter"
            '(?<$1>' . $allowedParamChars . ')', # with "(?<parameter>[a-zA-Z0-9\_\-]+)"
            $pattern
        );

        // Create capture group for '{parameter}'
        $pattern = preg_replace(
            '/{('. $allowedParamChars .')}/',    # Replace "{parameter}"
            '(?<$1>' . $allowedParamChars . ')', # with "(?<parameter>[a-zA-Z0-9\_\-]+)"
            $pattern
        );

        // Add start and end matching
        $patternAsRegex = "@^" . $pattern . "$@D";

        return $patternAsRegex;
    }

    /**
     * @param array $routes
     * @param bool $quitAfterRun
     * @return int
     */
    private static function handle(array $routes, $quitAfterRun = false)
    {
        // Counter to keep track of the number of routes we've handled
        $numHandled = 0;

        // The current page URL
        $uri = self::$request->getUri()->getPath();

        // Loop all routes
        foreach ($routes as $route) {
            // Make regexp from route
            $patternAsRegex = self::getRegex($route['pattern']);

            if ($ok = !!$patternAsRegex) {
                // We've got a regex, let's parse a URL
                if ($ok = preg_match($patternAsRegex, $uri, $matches)) {
                    // Get elements with string keys from matches
                    $params = array_intersect_key(
                        $matches,
                        array_flip(array_filter(array_keys($matches), 'is_string'))
                    );

                    self::invoke($route['callback'], $params);
                    ++$numHandled;

                    // If we need to quit, then quit
                    if ($quitAfterRun) {
                        break;
                    }
                }
            }
        }

        // Return the number of routes handled
        return $numHandled;
    }

    /**
     * @param $callable
     * @param array $params
     */
    private static function invoke($callable, $params = []) {
        if (is_callable($callable)) {
            self::$response = call_user_func_array($callable, [self::$request, self::$response, $params]);
        }
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    protected static function isEmptyResponse(ResponseInterface $response)
    {
        if (method_exists($response, 'isEmpty')) {
            return $response->isEmpty();
        }

        return in_array($response->getStatusCode(), [204, 205, 304]);
    }

    /**
     *
     */
    private static function respond()
    {
        // Send response
        if (!headers_sent()) {
            // Headers
            foreach (self::$response->getHeaders() as $name => $values) {
                foreach ($values as $value) {
                    header(sprintf('%s: %s', $name, $value), false);
                }
            }

            // Set the status _after_ the headers, because of PHP's "helpful" behavior with location headers.
            // See https://github.com/slimphp/Slim/issues/1730
            // Status
            header(sprintf(
                'HTTP/%s %s %s',
                self::$response->getProtocolVersion(),
                self::$response->getStatusCode(),
                self::$response->getReasonPhrase()
            ));
        }
        // Body
        if (!self::isEmptyResponse(self::$response)) {
            $body = self::$response->getBody();
            if ($body->isSeekable()) {
                $body->rewind();
            }

            $chunkSize = 4096;
            $contentLength = self::$response->getHeaderLine('Content-Length');

            if (!$contentLength) {
                $contentLength = $body->getSize();
            }
            if (isset($contentLength)) {
                $amountToRead = $contentLength;
                while ($amountToRead > 0 && !$body->eof()) {
                    $data = $body->read(min($chunkSize, $amountToRead));
                    echo $data;
                    $amountToRead -= strlen($data);
                    if (connection_status() != CONNECTION_NORMAL) {
                        break;
                    }
                }
            } else {
                while (!$body->eof()) {
                    echo $body->read($chunkSize);
                    if (connection_status() != CONNECTION_NORMAL) {
                        break;
                    }
                }
            }
        }
    }

    public static function run()
    {
        ob_start();
        // Handle all routes
        $numHandled = 0;

        if (isset(self::$routes[self::$request->getMethod()])) {
            $numHandled = self::handle(self::$routes[self::$request->getMethod()], true);
        }

        // If no route was handled, trigger the 404 (if any)
        if ($numHandled === 0) {
            $stream = StreamFactoryDiscovery::find();
            $data = [
                'success' => 'false',
                'reason' => 'Not Found!'
            ];

            self::$response = self::$response
                ->withBody($stream->createStream(json_encode($data)))
                ->withStatus(404, 'NOT FOUND')
                ->withHeader('Content-Type', 'application/json');
        }

        // If it originally was a HEAD request, clean up after ourselves by emptying the output buffer
        if (self::$request->getMethod() == 'HEAD') {
            ob_end_clean();
        }

        self::respond();

        // Return true if a route was handled, false otherwise
        return $numHandled !== 0;
    }

}