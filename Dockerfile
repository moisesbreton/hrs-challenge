FROM php:7.1-apache
RUN apt-get update && apt-get install -q -y sudo libicu-dev libpng-dev git openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile
RUN a2enmod rewrite
COPY . /var/www/html/
WORKDIR /var/www/html
RUN service apache2 restart
RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo
RUN chown -R docker:docker /var/www/html
EXPOSE 80
EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]