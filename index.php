<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 *
 * @author  Moises Breton
 * @license private
 */

// Setup configuration files
$dir = dirname(__DIR__);
$appDir = $dir . '/html/App';

DEFINE('APP_ROOT', $appDir);

require APP_ROOT . '/vendor/autoload.php';

use HRS\App\Route;

Route::init();

Route::resource('patients');
Route::resource('patients.metrics');

Route::run();