## HRS CODE CHALLENGE

Using modern PHP (think PHP7, composer, autoloading, PSR-4, and best practices) please create a "Routing Class" as defined below.

A routing class is designed to take a url segment and automatically invoke a controller class. It does this by breaking up the url segments that come
in a predictable pattern and invoking the correct class and method and inserting parameters into the method invoked. We will be following
a REST-ful convention and be using common http methods. Your Routing class will be aware of the http method and the actual url segment.

Here is the complete requirement. These tables represents all possible routing logic.

## Unnested routes 

| Method | Segment      | Controller class name | Controller method name | Parameters                                                      |
| ------ | ------------ | --------------------- | ---------------------- | --------------------------------------------------------------- |
| GET    | /patients    | PatientsController    | index                  | none                                                            |
| GET    | /patients/2  | PatientsController    | get                    | this should invoke `get($patientId)` where $patientId = 2       |
| POST   | /patients    | PatientsController    | create                 | none (extra credit for handling the request body)               |
| PATCH  | /patients/2  | PatientsController    | update                 | `update($patientId)`                                            |       
| DELETE | /patients/2  | PatientsController    | delete                 | `delete($patientId)`                                            |


## Nested routes

| Method | Segment                    | Controller class name         | Controller method name | Parameters                                  |
|------- | -------------------------- | ----------------------------- | ---------------------- | ------------------------------------------- |
| GET    | /patients/2/metrics        | PatientsMetricsController     | index                  | `index($patientId)`                         |
| GET    | /patients/2/metrics/abc    | PatientsMetricsController     | get                    | `get($patientId, $metricId)`                |
| POST   | /patients/2/metrics        | PatientsMetricsController     | create                 | `create($patientId)`                        |
| PATCH  | /patients/2/metrics/abc    | PatientsMetricsController     | update                 | `update($patientId, $metricId)`             |       
| DELETE | /patients/2/metrics/abc    | PatientsMetricsController     | delete                 | `delete($patientId, $metricId)`             |




To invoke the Routing class we'll be calling it statically. For example we'll be defininga `routes.php` that looks like this


```
Route::resource('patients');
Route::resource('patients.metrics');
```

##SOLUTION

The main file is in ```App/Route.php```, I used Slim Framework and Composer autoload using PSR-4.
The code is also wrote using PHP 7.

###RUN

```
composer install

docker-compose up -d --build
```

Test on localhost with:

http://localhost/patients
```
{"controller":"patients","action":"index","status":"OK"}
```

http://localhost/patients/2
```
{"controller":"patients","action":"get","id":"2","status":"OK"}
```

http://localhost/patients/4/metrics
```
{"controller":"patientsmetrics","action":"index","id":"4","status":"OK"}
```

http://localhost/patients/4/metrics/abc
```
{"controller":"patientsmetrics","action":"get","id":"4","id2":"abc","status":"OK"}
```